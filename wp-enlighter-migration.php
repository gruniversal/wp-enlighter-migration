<?php

/*
** Simple script to migrate WordPress Enlighter entries from one theme to another.
**
** Especially helpful for migrating old legacy themes (Git, Mocha, MooTools,
** Panic, Tutti, Twilight) which were removed in the current version.
**
** How to use:
** - configure old and new theme name
** - copy MySQL settings from wp-config.php or edit manually
** - run the script in your browser
** - check the suggested changes in dry-run mode
** - click the button to actually perform them (this is irreversible)
**
** Author: David Gruner, https://gruniversal.de
** GitLab: https://gitlab.com/gruniversal/wp-enlighter-migration
** License: CC BY-SA 4.0, https://creativecommons.org/licenses/by-sa/4.0/
**
**/

// enable reporting if you like

// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);

/*************************************************
*** this configuration has to be done manually ***
*************************************************/

// ** Theme settings **/

// You can determine the theme name in the WordPress code-editor:
// <!-- wp:enlighter/codeblock {"language":"php","theme":"mocha"} -->
//
// see also $valid_themes and $original_themes below

// name of the theme you want to replace
$theme_old = '';

// name of the theme you want to use instead
$theme_new = '';

/******************************************************
*** this configuration can be copied from wp-config ***
******************************************************/

// ** MySQL settings ** //
/** The name of the database for WordPress */
define( 'DB_NAME', '' );

/** MySQL database username */
define( 'DB_USER', '' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost:3306' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/********************************************************
*** there is no need to edit anything below this line ***
********************************************************/

// say hello

print "<b>WordPress Enlighter entries migration script v0.1</b><br>";

// theme information
$valid_themes = [
    // these are the same as in previous in the version
    'wpcustom', 'enlighter', 'beyond', 'classic', 'godzilla',
    'atomic', 'droide', 'minimal', 'eclipse', 'rowhammer',
    // this theme was renamed from "mootwo"
    'mowtwo',
    // these are new added themes
    'bootstrap4', 'dracula', 'monokai'
];
$original_themes = [
    // these are the same in the current version
    'wpcustom', 'enlighter', 'beyond', 'classic', 'godzilla',
    'atomic', 'droide', 'minimal', 'eclipse', 'rowhammer',
    // this theme was renamed to "mowtwo"
    'mootwo',
    // these themes were removed
    'git', 'mocha', 'mootools', 'panic', 'tutti', 'twilight'
];

// check if theme-settings are ok
if ( !in_array($theme_old, $original_themes) ) {
    print "WARNING - Theme '$theme_old' doesn't look like a original theme name.<br>";
}
if ( !in_array($theme_new, $valid_themes) ) {
    print "ERROR - Theme '$theme_new' is not a valid theme name.<br>";
    exit(1);
}

// tell the user what themes will be used
print "Migration: from theme <code>$theme_old</code> to theme <code>$theme_new</code>.<br>";

// check if MySQL settings are set
if ( empty(DB_NAME) || empty(DB_USER)) {
    print "ERROR - No database configuration: copy MySQL settings from wp-config.php or edit manually.<br>";
    exit(1);
}

// check if MySQL connection is possible
$db = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
if (!$db) {
    print "ERROR - Connection to database ".DB_NAME." failed: check database configuration.<br>";
    exit(1);
}

print "Database: <code>".DB_NAME."</code> on ".mysqli_get_host_info($db)." - using table prefix: <code>".$table_prefix."</code><br>";

/*****************************************************
*** dry-run mode - only display changes to perform ***
*****************************************************/

print "Analyzing database to find codeblocks to migrate.<br>";

// collect UPDATE statements for real run
$updates = array();

// retrieve entries to migrate
if ( ! $result = $db->query(
    "SELECT `ID`, `post_title`, `post_title`, `post_content`
    FROM `".$table_prefix."posts`
    WHERE `post_type` = 'post'
    AND `post_content` LIKE '%wp:enlighter/codeblock%'")
) {
    print "ERROR - false SQL statement in line ".__LINE__."<br>";
    exit(1);
}

// tell the user if something was found
$count_entries = $result->num_rows;
print "Found $count_entries posts with Enlighter codeblocks.<br>";

// no entries -> no action needed
if ( 0 === $count_entries ) {
    print "Nothing to migrate.<br>";
    exit;
}

// loop through all entries to detect if they need to be changed
$entries = $result->fetch_all(MYSQLI_ASSOC);
foreach ($entries as $id => $entry) {
    print "Post ".$entry['ID'].": ".$entry['post_title']."<br>";

    // all operations are done directly on $new_entry
    $new_entry = $entry['post_content'];

    // regexp 1
    $pattern = '/<!-- wp:enlighter\/codeblock {.*"theme":"'.$theme_old.'".*} -->/';
    $replace = '<!-- wp:enlighter/codeblock {\1"theme":"'.$theme_new.'"\2} -->';
    $new_entry = preg_replace($pattern, $replace, $new_entry, 1000, $count_regexp1);

    // regexp 2
    $pattern = '/data-enlighter-theme="'.$theme_old.'"/';
    $replace = 'data-enlighter-theme="'.$theme_new.'"';
    $new_entry = preg_replace($pattern, $replace, $new_entry, 1000, $count_regexp2);

    // number of replacements must be equal
    if ($count_regexp1 !== $count_regexp2) {
        print "ERROR - number of replacements must be equal in line ".__LINE__."<br>";
        exit(1);
    }
    // found something to migrate -> generate UPDATE statement
    if ( $count_regexp1 > 0 ) {
        print " -- $count_regexp1 codeblocks to migrate<br>";

        $updates[] = "UPDATE `".$table_prefix."posts` SET `post_content` = '".$db->escape_string($new_entry)."' WHERE `ID` = '".$entry['ID']."'";
    }
}

// tell the user what need to be done
$count_updates = count($updates);
print "Created $count_updates database update statements to preform the migration.<br>";
if ( 0 === $count_updates ) {
    print "Nothing to migrate.<br>";
    exit;
}

/*********************************************************
*** real-run mode - changes will be actually performed ***
*********************************************************/

print "<hr>";

// generate hash to ensure analyze info was displayed before
$hash = md5(serialize($updates));

// if hash equals POST data, run the migration
if ( isset($_POST['perform_updates']) && $hash === $_POST['perform_updates'] ) {
    print "<b>Performing Updates </b>";

    foreach($updates as $sql) {
        $result = $db->query($sql);
        if (FALSE === $result) {

            print "ERROR - ".$db->error."<br>";
            exit(1);
        }
        print ".";
    }

    print "<br>";
    print "All changes were made. You should now check your site if everything is fine.";

    exit;
}

print "<b>WARNING: This is an irreversible action.</b><br>";
print "<b>This script changes entries in your wordpress database.</b><br>";
print "<b>Ensure you have a database backup if something goes wild!</b><br>";

print '<form action="'.$_SERVER['SCRIPT_NAME'].'" method="post">';
print '<input type="submit" value="Perform '.$count_updates.' sql update statements on my database to migrate the theme."></input>';
print '<input type="hidden" name="perform_updates" value="'.$hash.'"></input>';
print '</form>';
