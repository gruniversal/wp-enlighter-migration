# WordPress Enlighter Migration (wp-enlighter-migration) #

Simple script to migrate [WordPress Enlighter](https://de.wordpress.org/plugins/enlighter/:) entries from one theme to another.

Especially helpful for migrating old legacy themes (Git, Mocha, MooTools, Panic, Tutti, Twilight) which were removed in the [current version](https://github.com/EnlighterJS/documentation/blob/master/wordpress/upgrade/v4.md).

Author:
- David Gruner, https://www.gruniversal.de

License:
- Creative Commons BY-SA 4.0: https://creativecommons.org/licenses/by-sa/4.0/

## How to use ##

- configure old and new theme name
- copy MySQL settings from wp-config.php or edit manually
- run the script in your browser
- check the suggested changes in dry-run mode
- click the button to actually perform them (this is irreversible)

## Warning ##

The code is very straightforward and mainly based on my personal needs.

I've added some validity checks and warnings to prevent some problems, but it's still pretty alpha, so be sure to have a backup of your database.

## Changelog ##

### v0.1 (21.04.2020) ###

Initial Release
- detects valid theme names
- dry-run mode
- some error handling
